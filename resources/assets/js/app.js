
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('passwordeye', require('./components/PasswordEye.vue'));

const app = new Vue({
  el: '#app',

  data: {
    password:         '',
    passwordConfirm:  '',
    canSubmit:        false,
    settingsSubmitButtonClass: {
      'btn':        true,
      'btn-primary': true,
      'active':     false,
      'disabled':   true,
      'manage-btn': true
    }
  },

  methods: {
    checkConfirmPassword: function() {
      let errorMsg = document.getElementById('password-not-same');

      if (this.password !== this.passwordConfirm) {
        errorMsg.style.display = 'block';
        this.disableSubmitButton();
      } else {
        errorMsg.style.display = 'none';
        this.enableSubmitButton();
      }
    },

    disableSubmitButton: function() {
      console.log('inside disable');
      this.settingsSubmitButtonClass.disabled = true;
      this.settingsSubmitButtonClass.active = false;
      this.canSubmit = false;
    },

    enableSubmitButton: function() {
      this.settingsSubmitButtonClass.disabled = false;
      this.settingsSubmitButtonClass.active = true;
      this.canSubmit = true;
    },

    startSettingsSubmission: function(e) {
      let email = document.getElementById('email');
      if ( email.value.length === 0 ) {
        this.canSubmit = false;
      }

      if ( !this.canSubmit ) {
        e.preventDefault();
      }
    }
  }
});
