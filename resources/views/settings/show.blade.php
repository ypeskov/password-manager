<div class="panel panel-default">
  <div class="panel-heading">
    {{ __('app.manage_settings') }}</strong>

    @include('common.back', ['backRoute' => 'dashboard'])
  </div>

  <div class="panel-body">
    <form name="settings"
          v-on:submit="startSettingsSubmission($event)"
          action="{{ route('update-settings') }}"
          method="POST">
      {{ csrf_field() }}

      <div class="form-group">
        <div class="row resource-item-line">
          <div class="col-md-3">
            <label for="name">{{ mb_ucfirst(__('app.user_name')) }}</label>
          </div>
          <div class="col-md-9">
            <input id="name"
                   class="form-control"
                   type="text"
                   placeholder="{{ mb_ucfirst(__('app.enter_name')) }}"
                   name="name"
                   value="{{ $user->name }}" />
          </div>
        </div>

        <div class="row resource-item-line">
          <div class="col-md-3">
            <label for="email">{{ mb_ucfirst(__('app.email')) }}</label>
          </div>
          <div class="col-md-9">
            <input id="email"
                   class="form-control"
                   placeholder="{{ mb_ucfirst(__('app.enter_email')) }}"
                   type="email"
                   name="email"
                   value="{{ $user->email }}" />
          </div>
        </div>

        <div class="row resource-item-line">
          <div class="col-md-3">
            <label for="password">{{ mb_ucfirst(__('app.password')) }}</label>
          </div>
          <div class="col-md-9">
            <input id="password"
                   class="form-control"
                   @keyup="checkConfirmPassword()"
                   placeholder="{{ mb_ucfirst(__('app.enter_password')) }} {{ __('app.min_6_symbols') }}"
                   type="password"
                   name="password"
                   v-model="password" />
          </div>
        </div>

        <div class="row resource-item-line">
          <div class="col-md-3">
            <label for="confirm-password">{{ mb_ucfirst(__('app.confirm_password')) }}</label>
          </div>
          <div class="col-md-9">
            <input id="confirm-password"
                   @keyup="checkConfirmPassword()"
                   placeholder="{{ mb_ucfirst(__('app.repeat_password')) }}"
                   class="form-control"
                   type="password"
                   name="confirm-password"
                   v-model="passwordConfirm" />
          </div>
        </div>

        <div class="row">
          <div class="col-md-3"></div>
          <div id="password-not-same"
               class="col-md-9 password-not-same">{{ __('app.password_not_same_as_confirm') }}</div>
        </div>

        <hr />

        <button id="submit-settings"
                type="submit"
                v-bind:class="settingsSubmitButtonClass">{{ mb_ucfirst(__('app.submit')) }}</button>
      </div>
    </form>
  </div>
</div>