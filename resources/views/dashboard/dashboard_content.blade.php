<div class="row">
  <div class="col-md-6">
    <img src="/images/password.png" class="dashboard-icon" />
    <a href="{{ route('passwords-list') }}" class="dashboard-link">{{ mb_ucfirst(__('app.passwords')) }}</a>
  </div>

  <div class="col-md-6">
    <img src="/images/cc.png" class="dashboard-icon" />
    <a href="{{ route('begin') }}" class="dashboard-link">{{ mb_ucfirst(__('app.credit_cards')) }}</a>
  </div>
</div>