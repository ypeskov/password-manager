<div class="panel panel-default">
    <div class="panel-heading">{{ mb_ucfirst(__('app.dashboard')) }}</div>

    <div class="panel-body">
      @include('dashboard.dashboard_content')
    </div>
</div>
