@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">
            {{ mb_ucfirst(__('app.password_resource')) }}

            @include('common.back', ['backRoute' => 'passwords-list'])
          </div>

          <div class="panel-body">
            <div class="row resource-item-line">
              <div class="col-md-3">{{ mb_ucfirst(__('app.resource_name')) }}</div>
              <div class="col-md-9"><strong>{!! $resource->resource !!}</strong></div>
            </div>

            <div class="row resource-item-line">
              <div class="col-md-3">{{ mb_ucfirst(__('app.resource_login')) }}</div>
              <div class="col-md-9"><strong>{{ $resource->login }}</strong></div>
            </div>

            <div class="row resource-item-line">
              <div class="col-md-3">{{ mb_ucfirst(__('app.password')) }}</div>
              <div class="col-md-9">
                <span>
                  <passwordeye resource-id="{{ $resource->id }}"></passwordeye>
                </span>
              </div>
            </div>

            <div class="row resource-item-line">
              <div class="col-md-3">{{ mb_ucfirst(__('app.comment')) }}</div>
              <div class="col-md-9"><strong>{{ $resource->comment }}</strong></div>
            </div>

            <hr />

            <a href="{{ route('passwords-form') }}?isEdit=true&resourceId={{ $resource->id }}"
               class="btn btn-primary manage-btn">
              {{ mb_ucfirst(__('app.edit')) }}
            </a>

            <a href="{{ route('password-confirm-delete', $resource->id) }}"
               class="btn btn-danger manage-btn">
              {{ mb_ucfirst(__('app.delete')) }}
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection