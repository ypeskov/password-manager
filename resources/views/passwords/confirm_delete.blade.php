<div class="panel panel-default">
  <div class="panel-heading">
    {{ __('app.confirm_delete_resource') }}: <strong>{{ $resource['resource'] }}</strong>
  </div>

  <div class="panel-body">
    <a href="{{ route('password-show', $resource['id']) }}"
       class="btn btn-primary manage-btn">{{ mb_ucfirst(__('cancel')) }}</a>

    <a href="{{ route('password-delete', $resource['id']) }}"
       class="btn btn-danger manage-btn">{{ mb_ucfirst(__('delete')) }}</a>
  </div>
</div>
