<div class="panel panel-default">
  <div class="panel-heading">
    {{ $formActionName }}

    <div class="back-link">
      <a href="{{ $backRoute }}">{{ mb_ucfirst(__('app.back')) }}</a>
    </div>
  </div>

  <div class="panel-body">
    <form method="POST" action="{{ route('password->save') }}">
      {{ csrf_field() }}

      <input type="hidden" name="is_edit" value="{{ $isEdit }}" />
      <input type="hidden" name="resource_id" value="{{ $resource->id }}" />

      <div class="form-group">
        <div class="row resource-item-line">
          <div class="col-md-3">
            <label for="resourceName">{{ mb_ucfirst(__('app.resource_name')) }}</label>
          </div>
          <div class="col-md-9">
            <input id="resourceName"
                   type="text"
                   class="form-control"
                   name="resourceName"
                   value="{{ $resource->resource }}"
                   placeholder="{{ __('app.enter_resource_name') }}" />
          </div>
        </div>

        <div class="row resource-item-line">
            <div class="col-md-3">
              <label for="login">{{ mb_ucfirst(__('app.resource_login')) }}</label>
            </div>
            <div class="col-md-9">
              <input id="login"
                     type="text"
                     class="form-control"
                     name="login"
                     value="{{ $resource->login }}"
                     placeholder="{{ __('app.enter_login') }}"/>
            </div>
        </div>

        <div class="row resource-item-line">
          <div class="col-md-3">
            <label for="password">{{ mb_ucfirst(__('app.password')) }}</label>
          </div>
          <div class="col-md-9">
            <input id="password"
                   type="text"
                   class="form-control"
                   name="password"
                   value="{{ $resource->password }}"
                   placeholder="{{ __('app.enter_password') }}" />
          </div>
        </div>

        <div class="row resource-item-line">
          <div class="col-md-3">
            <label for="comment">{{ mb_ucfirst(__('app.comment')) }}</label>
          </div>
          <div class="col-md-9">
            <textarea id="comment"
                      class="form-control"
                      name="comment"
                      placeholder="{{ __('app.enter_comment') }}">{{ $resource->comment }}</textarea>
          </div>
        </div>

        <hr />

        <button type="submit"
                class="btn btn-primary manage-btn">{{ mb_ucfirst(__('app.submit')) }}</button>
      </div>
    </form>
  </div>
</div>
