<div class="panel panel-default">
  <div class="panel-heading">
    <div class="row">
      <div class="col-sm-12 panel-heading-text">
        @include('common.back', ['backRoute' => 'dashboard'])

        {{ mb_ucfirst(__('app.passwords')) }}
          &nbsp;<span class="password-resource-total">({{ mb_ucfirst(__('app.total')) }}: {{ sizeof($resources) }})&nbsp;</span>
      </div>
    </div>
  </div>

  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <a  href="{{ route('passwords-form') }}"
            class="btn btn-primary new-resource-button">{{ mb_ucfirst(__('app.new')) }}
        </a>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        @if ( sizeof($resources) > 0 )
          @foreach($resources as $resource)
            <div class="btn-group-vertical password-resource-wrapper" role="group" style="width: 100%;">
              <a class="btn btn-lg btn-block btn-default"
                 href="{{ route('password-show', $resource->id) }}">{{ $resource->resource }}
              </a>
            </div>
          @endforeach
        @else
          {{ mb_ucfirst(__('app.no_resources_available')) }}
        @endif
      </div>
    </div>

    </div>
</div>