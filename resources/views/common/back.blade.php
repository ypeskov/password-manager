<div class="back-link">
  <a href="{{ route($backRoute) }}">{{ mb_ucfirst(__('app.back')) }}</a>
</div>