@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        @if ( sizeof($errors) > 0)
          <div class="alert alert-danger">
            <ul>
              @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

        @if ( session('status') )
          <div class="alert alert-success">{!! session('status') !!}</div>
        @endif
      </div>
    </div>

    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        @include($panelTemplate)
      </div>
    </div>
  </div>
@endsection