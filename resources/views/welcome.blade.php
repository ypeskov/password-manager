@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p>{{ __('app.description') }}</p>
        <p>{{ __('app.you_can') }} or
          <a href="{{ route('login') }}">{{ __('app.login') }}</a>
          or
          <a href="{{ route('register') }}">{{ __('app.register') }}</a>
        </p>
      </div>
    </div>
  </div>
@endsection