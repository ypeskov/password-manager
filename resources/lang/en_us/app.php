<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 04.04.17
 * Time: 22:27
 */

return [
    'description'    => <<<EOL
    The Password Manager is aimed for personal usage. You can use it if you want without any warranty.
EOL
    ,
    'you_can'   => 'You can',
    'login'     => 'login',
    'register'  => 'register',
    'dashboard' => 'dashboard',
    'passwords' => 'passwords',
    'credit_cards'  => 'credit cards',
    'no_resources_available'    => 'No resources are available',
    'password_resource'     => 'password resource',
    'resource_not_found'    => 'password resource is not found',
    'resource_name'     => 'resource name',
    'resource_login'    => 'resource login',
    'password'  => 'password',
    'comment'   => 'comment',
    'total'     => 'total',
    'back'      => 'back',
    'new'       => 'new',
    'create_password'   => 'create password',
    'submit'    => 'submit',
    'enter_comment'         => 'Enter your comment here',
    'enter_resource_name'   => 'Enter resource name',
    'enter_login'           => 'Enter login',
    'enter_password'        => 'Enter password',
    'resource_created'      => 'Resource has been created',
    'resource_updated'      => 'Resource has been updated',
    'edit'      => 'edit',
    'delete'    =>'delete',
    'confirm_delete_resource'   => 'Do You really want to delete the resource',
    'resource_deleted'  => 'Resource has been deleted',
    'settings'  => 'settings',
    'manage_settings'   => 'Manage Settings',
    'user_name' => 'user name',
    'email' => 'email',
    'confirm_password'  => 'confirm password',
    'repeat_password'   => 'repeat password',
    'enter_name'    => 'enter name',
    'enter_email'   => 'enter email',
    'min_6_symbols' => 'at least 6 symbols',
    'password_not_same_as_confirm'  => 'Password and password confirmation must be the same',
    'settings_updated'  => 'Settings were updated',
];