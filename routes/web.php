<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'EntryController@index')->name('begin');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::get('/settings', 'SettingsController@index')->name('settings');
Route::post('/settings/update', 'SettingsController@updateSettings')->name('update-settings');

Route::group(['prefix' => 'passwords'], function() {
    Route::get('/list', 'PasswordManagerController@listPasswords')->name('passwords-list');

    Route::get('/password-form', 'PasswordManagerController@passwordForm')->name('passwords-form');

    Route::post('/password-save', 'PasswordManagerController@savePassword')->name('password->save');

    Route::get('/password-confirm-delete/{resourceId}', 'PasswordManagerController@confirmDelete')
        ->name('password-confirm-delete')
        ->where('resourceId', '\d');

    Route::get('/password-delete/{resourceId}', 'PasswordManagerController@deletePassword')
        ->name('password-delete')
        ->where('resourceId', '\d');

    Route::get('/show/{resourceId}', 'PasswordManagerController@showPassword')
        ->name('password-show')
        ->where('resourceId', '\d+');

    Route::get('/get-password/{id}', 'PasswordManagerController@getPassword')
        ->name('get-password')
        ->where('id', '\d+');
});
