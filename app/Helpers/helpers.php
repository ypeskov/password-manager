<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 05.04.17
 * Time: 9:09
 */

/**
 * Returns the $string capitalized with the first letter.
 *
 * @param   string $string
 *
 * @return  String
 */
if ( !function_exists('mb_ucfirst') && function_exists('mb_substr')) {
    function mb_ucfirst($string) {
        $string = mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);

        return $string;
    }
}

/**
 * Builds and returns html <a> if the string begins from http:// or https://
 *
 * @param string $str
 * @return string
 */
if ( !function_exists('getURLIfRequired') ) {
    function getURLIfRequired(string $str)
    {
        $isHttpAt   = mb_strpos($str, 'http://');
        $isHttpsAt  = mb_strpos($str, 'https://');

        if ( (is_int($isHttpAt) && $isHttpAt === 0)
            or
            (is_int($isHttpsAt) && $isHttpsAt === 0) ) {

            $str = '<a href="' . $str . '" target="_blank">' . $str . '</a>';
        }

        return $str;
    }
}