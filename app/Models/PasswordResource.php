<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Defuse\Crypto\Crypto;

class PasswordResource extends Model
{
    protected $fillable = [
        'user_id',
        'resource',
        'login',
        'password',
        'comment',
    ];

    /**
     * @param int $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    static public function getAllOwnResources(int $userId)
    {
        return
            self::where([
                ['user_id', '=', $userId]
            ])
            ->orderBy('resource')
            ->get();
    }

    /**
     * @param   int $id
     * @param   int $userId
     * @return  \Illuminate\Database\Eloquent\Collection
     */
    static public function getOwnResource(int $id, int $userId)
    {
        return
            self::where([
                ['id', '=', $id],
                ['user_id', '=', $userId]
            ])
            ->get();
    }

    /**
     * @param Request $request
     * @param int $userId
     * @param string $key
     * @return \App\Models\PasswordResource
     */
    static public function createPasswordResource(Request $request, int $userId, string $key)
    {
        if ( !$request->comment ) {
            $request->comment = '';
        }

        $passEncrypted = Crypto::encryptWithPassword($request->password, $key);

        return self::create([
            'resource'  => $request->resourceName,
            'user_id'   => $userId,
            'login'     => $request->login,
            'password'  => $passEncrypted,
            'comment'   => $request->comment
        ]);
    }

    /**
     * @param Request $request
     * @param int $userId
     * @param string $key
     * @return bool
     */
    static public function updatePasswordResource($resourceId, Request $request, int $userId, string $key)
    {
        if ( !$request->comment ) {
            $request->comment = '';
        }

        $passEncrypted = Crypto::encryptWithPassword($request->password, $key);

        return self::where('id', $resourceId)
            ->update([
            'resource'  => $request->resourceName,
            'user_id'   => $userId,
            'login'     => $request->login,
            'password'  => $passEncrypted,
            'comment'   => $request->comment
        ]);
    }

    /**
     * @param array $requestData
     * @param bool $isEdit
     * @return mixed
     */
    static public function getValidator(array $requestData, bool $isEdit=false)
    {
        $resourceNameRule = 'required|min:1';
        if ( !$isEdit ) {
           $resourceNameRule = $resourceNameRule . '|unique:password_resources,resource';
        }

        $validationRules =[
            'resourceName'  => $resourceNameRule,
            'login'         => 'required',
            'password'      => 'required',
        ];

        return Validator::make($requestData, $validationRules);
    }

    /**
     * @param int $userId
     * @param string $oldKey
     * @param string $newKey
     */
    static public function reEncryptUsersPasswords(int $userId, string $oldKey, string $newKey)
    {
        $passwordResources = self::where('user_id', '=', $userId)->get();

        foreach($passwordResources as $res) {
            $password = Crypto::decryptWithPassword($res->password, $oldKey);

            $res->password = Crypto::encryptWithPassword($password, $newKey);

            $res->save();
        }
    }
}
