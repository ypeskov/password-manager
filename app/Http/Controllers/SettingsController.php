<?php

namespace App\Http\Controllers;

use App\Models\PasswordResource;
use Illuminate\Http\Request;
use \Auth;
use \DB;
use App\User;
use Symfony\Component\HttpFoundation\Session\Session;

class SettingsController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();

        $data = [
            'panelTemplate' => 'settings.show',
            'user'  => $user,
        ];

        return view('common.panel', $data);
    }

    public function updateSettings(Request $request)
    {
        $user       = Auth::user();
        $session    = new Session();

        $validator = User::getValidator($request->all(), $user);

        if ( $validator->fails() ) {
            return redirect(route('settings'))
                ->withErrors($validator);
        }

        //Encrypt all passwords with the new login password
        //Make it in transaction in case if something fails
        DB::transaction(function() use ($user, $request, $session) {
            $user->name     = $request->name;
            $user->email    = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            PasswordResource::reEncryptUsersPasswords($user->id,
                $session->get(config('app.sessionKey')), $request->password);

            //update session key with new value
            $session->set(config('app.sessionKey'), $request->password);
        });

        return redirect(route('dashboard'))
            ->with('status', __('app.settings_updated'));
    }
}
