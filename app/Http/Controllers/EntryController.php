<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EntryController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest');
    }

    public function index()
    {
        return view('welcome');
    }
}
