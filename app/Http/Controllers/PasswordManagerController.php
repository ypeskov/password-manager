<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PasswordResource;
use \Auth;
use Defuse\Crypto\Crypto;
use Symfony\Component\HttpFoundation\Session\Session;

class PasswordManagerController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth');
    }

    public function listPasswords()
    {
        $resources = PasswordResource::getAllOwnResources(Auth::id());

        $data = [
            'panelTemplate' => 'passwords.list',
            'resources'     => $resources,
        ];

        return view('common.panel', $data);
    }

    /**
     * @param int $resourceId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function showPassword(int $resourceId)
    {
        $resource = PasswordResource::getOwnResource($resourceId, Auth::id());

        if ( sizeof($resource) === 0 ) {
            return redirect(route('passwords-list'));
        }

        $resource = $resource[0];
        $resource->password = "*****";

        $resource->resource = getURLIfRequired($resource->resource);

        $data = [
            'panelTemplate' => 'passwords.show',
            'resource'      => $resource,
        ];

        return view('common.panel', $data);
    }

    /**
     * @param int $id
     * @return string
     */
    public function getPassword(Request $request, int $id)
    {
        $resource = PasswordResource::getOwnResource($id, Auth::id());

        if ( sizeof($resource) === 0 ) {
            return [];
        }

        $session = new Session();

        return Crypto::decryptWithPassword($resource[0]->password,
                                    $session->get(config('app.sessionKey')));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function passwordForm(Request $request)
    {
        $session = new Session();

        $resource = new PasswordResource();
        $isEdit = false;
        $backRoute  = route('passwords-list');

        if ( ($request->isEdit && $request->isEdit === 'true')
            && $request->resourceId ) {

            $resource = PasswordResource::getOwnResource($request->resourceId, Auth::id());

            if ( sizeof($resource) === 1 ) {
                $resource   = $resource[0];

                $resource->password =
                    Crypto::decryptWithPassword($resource->password,
                        $session->get(config('app.sessionKey')));

                $isEdit     = true;
                $backRoute  = route('password-show', $request->resourceId);
            }
        }

        $formActionName = mb_ucfirst(__('app.create_password'));

        $data = [
            'panelTemplate' => 'passwords.edit_form',
            'resource'      => $resource,
            'isEdit'        => $isEdit,
            'backRoute'     => $backRoute,
            'formActionName'=> $formActionName,
        ];

        return view('common.panel', $data);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function savePassword(Request $request)
    {
        $key = (new Session())->get(config('app.sessionKey'));
        $paramStr = '';
        $successMsg = __('app.resource_created') . ': <strong>' . $request->resourceName . '</strong>';

        if ( (int) $request->is_edit === 1 ) {
            $paramStr = "?isEdit=true&resourceId=" . $request->resource_id;
            $validator =PasswordResource::getValidator($request->all(), true);
        } else {
            $validator =PasswordResource::getValidator($request->all());
        }

        if ( $validator->fails() ) {
            return redirect(route('passwords-form') . $paramStr)
                ->withErrors($validator);
        }

        //check that request has come to own password
        try {
            PasswordResource::where([
                ['id', '=', $request->resource_id],
                ['user_id', '=', Auth::id()]
            ])->firstOrFail();
        } catch (\Exception $e) {
            redirect(route('dashboard'));
        }

        if ( (int) $request->is_edit === 1 ) {
            PasswordResource::updatePasswordResource($request->resource_id, $request, Auth::id(), $key);
            $successMsg = __('app.resource_updated')
                . ': <strong>' . $request->resourceName . '</strong>';
        } else {
            PasswordResource::createPasswordResource($request, Auth::id(), $key);
        }


        return redirect(route('passwords-list'))
            ->with('status', $successMsg);
    }

    /**
     * @param int $resourceId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function confirmDelete(int $resourceId)
    {
        $resource = PasswordResource::getOwnResource($resourceId, Auth::id())
            ->toArray();

        if ( sizeof($resource) === 1 ) {
            $resource = $resource[0];
            unset($resource['password']);
        } else {
            return redirect(route('passwords-list'));
        }

        $data = [
            'panelTemplate' => 'passwords.confirm_delete',
            'resource'      => $resource,
        ];

        return view('common.panel', $data);
    }

    /**
     * @param int $resourceId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deletePassword(int $resourceId)
    {
        $resource = PasswordResource::where([
            ['id', '=', $resourceId],
            ['user_id', '=', Auth::id()]
        ])->firstOrFail();

        if (sizeof($resource) === 1) {
            $resource->delete();
        } else {
            return redirect(route('dashboard'));
        }

        return redirect(route('passwords-list'))
            ->with('status', __('app.resource_deleted') . ': <strong>' . $resource->resource . '</strong>');
    }
}
