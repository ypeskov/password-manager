<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class User extends Authenticatable
{
    use Notifiable,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    static public function getValidator(array $userData, $currentUser)
    {
        $emailValidateRules = 'required';

        if ( $currentUser->email !== $userData['email'] ) {
            $emailValidateRules .= '|unique:users,email';
        }

        $validationRules =[
            'name'      => 'min:1|required',
            'email'     => $emailValidateRules,
            'password'  => 'required|min:6',
        ];

        return Validator::make($userData, $validationRules);
    }

}
