<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'Yuriy',
            'email'     => 'yuriy.peskov@gmail.com',
            'password'  => bcrypt('123123'),
        ]);

        User::create([
            'name'      => 'Yuriy aka ypeskov@pisem.net',
            'email'     => 'ypeskov@pisem.net',
            'password'  => bcrypt('123123'),
        ]);
    }
}
