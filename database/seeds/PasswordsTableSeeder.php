<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\PasswordResource;
use Defuse\Crypto\Crypto;

class PasswordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::where('email', 'yuriy.peskov@gmail.com')->firstOrFail();
        $user2 = User::where('email', 'ypeskov@pisem.net')->firstOrFail();

        $key = '123123';
        $passEncrypted = Crypto::encryptWithPassword('123123', $key);

        PasswordResource::create([
            'user_id'   => $user1->id,
            'resource'  => 'www.example.com',
            'login'     => 'example',
            'password'  => $passEncrypted,
            'comment'   => 'Lorem ipsum galerus',
        ]);

        $passEncrypted = Crypto::encryptWithPassword('qwerty', $key);
        PasswordResource::create([
            'user_id'   => $user1->id,
            'resource'  => 'www.google.com',
            'login'     => 'google_user',
            'password'  => $passEncrypted,
            'comment'   => 'Das is comment',
        ]);

        $passEncrypted = Crypto::encryptWithPassword('pass', $key);
        PasswordResource::create([
            'user_id'   => $user1->id,
            'resource'  => 'www.yahoo.com',
            'login'     => 'yahoo_user',
            'password'  => $passEncrypted,
            'comment'   => 'Parqagwai',
        ]);

        $passEncrypted = Crypto::encryptWithPassword('msiao password', $key);
        PasswordResource::create([
            'user_id'   => $user2->id,
            'resource'  => 'msiao',
            'login'     => 'msiao_user',
            'password'  => $passEncrypted,
            'comment'   => '',
        ]);

        $passEncrypted = Crypto::encryptWithPassword('overflow password', $key);
        PasswordResource::create([
            'user_id'   => $user2->id,
            'resource'  => 'stackoverflow',
            'login'     => 'overflow_user',
            'password'  => $passEncrypted,
            'comment'   => 'Kulibaka baka baka',
        ]);
    }
}
